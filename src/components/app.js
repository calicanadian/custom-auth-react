import React, { Component } from 'react';
import axios from 'axios';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './Home';
import Dashboard from './Dashboard';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      loggedInStatus: "Logged Out",
      user: {}
    };

    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  checkLoginStatus() {
    axios.get("http://localhost:3001/logged_in", { withCredentials: true }).then(response => {
      if (response.data.logged_in && this.state.loggedInStatus === "Logged Out") {
        this.setState({
          loggedInStatus: "Logged In",
          user: response.data.user
        });
      } else if (!response.data.logged_in && this.state.loggedInStatus == "Logged In") {
        this.setState({
          loggedInStatus: "Logged Out",
          user: {}
        });
      }
    }).catch(error => {
      console.log("check login error", error);
    });
  }

  componentDidMount() {
    this.checkLoginStatus();
  }

  handleLogin(data) {
    this.setState({
      loggedInStatus: "Logged In",
      user: data.user
    });
  }

  handleLogout() {
    this.setState({
      loggedInStatus: "Logged Out",
      user: {}
    });
  }

  render() {
    return (
      <div className='app'>
        <BrowserRouter>
          <Switch>
            <Route
            exact
            path={'/'}
            render={props =>(
              <Home {...props} handleLogout={this.handleLogout} handleLogin={this.handleLogin} loggedInStatus={this.state.loggedInStatus} />
            )}
            />
            <Route
            exact
            path={'/dashboard'}
            render={props =>(
              <Dashboard {...props} loggedInStatus={this.state.loggedInStatus} />
            )}
            />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}
